const students = [{
  name: 'Dima',
  surname: 'Portyanka',
  age: 22,
  birthDate: '29.03.1994',
  daysWithoutChocolate: 4,
  hobbies: ['linux', 'js', 'films', 'open source', 'startups'],
  firstProgrammLang: 'pascal',
  langList: ['js', 'es6', 'php', 'c++', 'c#', 'pascal', 'erlang'],
  markupLang: ['hmtl', 'jade', 'mustache'],
  favouriteGames: ['mk', 'taken', 'nfs'],
  academicDegree: {
    level: 'magister',
    field: 'computer science'
  },
  smoke: false,
  address: {
    street: 'Furmanova',
    house: '8'
  },
  phone: '+380664767225',
  socialLinks: [{
    socialNetwork: 'skype',
    userName: 'stinkymark1'
  }, {
    socialNetwork: 'meetup',
    userName: '183450085'
  }, {
    socialNetwork: 'viber',
    userName: '+380664767225'
  }]
}, {
  name: 'Zhenia',
  surname: 'Vrubel',
  age: 36,
  birthDate: '11.08.1980',
  daysWithoutChocolate: 1,
  hobbies: ['travel', 'bike', 'brit_rock', 'makarame'],
  firstProgrammLang: 'python',
  langList: ['python', 'R', 'js'],
  markupLang: ['hmtl'],
  favouriteGames: ['erudit', 'tetris'],
  academicDegree: {
    level: 'magister',
    field: 'economics'
  },
  smoke: false,
  address: {
    street: 'Dzerzhinskogo',
    house: '24'
  },
  phone: '+380957552253',
  socialLinks: [{
    socialNetwork: 'skype',
    userName: 'vzhenia'
  }, {
    socialNetwork: 'facebook',
    userName: 'zhenia.vrubel'
  }, {
    socialNetwork: 'LinkedIn',
    userName: 'evgeniavrubel'
  }, {
    socialNetwork: 'viber',
    userName: '+380957552253'
  }, {
    userName: 'vladyslav',
  }]
}];

/**
 * [eateChocolate функция которая обновлеяет свойство ел ли человек шоколад]
 * @param  {object} who    [объект с полем имя фамилия чтобы идентифицирвать]
 * @param  {array} people [массив в котором будем менять статус человек что ел шоколад]
 * @return {array}        [возвращаем массив всех людей с уже измененным человеком]
 */

function eateChocolate(who = {name: '', surname: ''}, people = []) {
  return people.map((person) => {
    if (who.name === person.name && who.surname === person.surname) {
      return Object.assign({}, person, {daysWithoutChocolate: 0});
    }

    return person;
  })
}

console.log(
  eateChocolate({
    name: 'Dima',
    surname: 'Portyanka'
  }, students)
);

function renameStreet(arr){

//list of streets being renamed and not
var streetAssign = ['Arsenicheva, street', 'Yavornitskogo, lane', 'Blagoeva, street', 'Chkalova street', 'Dzerzhinskogo street', 'Engels, street', 'Furmanov, street', 'General Pear street', 'Horky street', 'Ilyich, street', 'Karla Marksa Prospekt', 'Lenin street', 'Moscow, street', 'October, Park, square', 'Shevchenko, street', 'Parkhomenko street', 'Rogaleva street', 'State farm, the street','Juliusha Slovatskogo, street'];
//list of old names of the streets which were renamed
var streetOld = ['Arsenicheva, street', 'Blagoeva, street', 'Chkalova street', 'Dzerzhinskogo street', 'Engels, street', 'Furmanov, street', 'General Pear street', 'Horky street', 'Ilyich, street', 'Karla Marksa Prospekt', 'Lenin street', 'Moscow, street', 'October, Park, square', 'Parkhomenko street', 'Rogaleva street', 'State farm, the street', 'Tkachenko street', 'Voroshilova street', 'Young Leninzi, street'];
//list of new names of the streets which were renamed
var streetNew = ['Alexander Reinhard', 'Sergei Podolynsky', 'Svyatoslav the Brave, street', 'Vladimir Vernadsky', 'Afanasyeva-Chuzhbinskogo', 'Cool Characters', 'Hetman Petro Doroshenko', 'Princess Olga', 'Pylyp Orlyk', 'Dmytro Yavornytsky', 'VOSKRESENSKA', 'Vladimir Monomakh', 'Ivan Starov', 'Gregory Hrabianka', 'Krutogorny, descent', 'Nestor Makhno', 'Vadim Sure', 'Sergey Efremov', 'Cossack Mamai'];
var assignedList = [];
var newList = [];

//randomly assign street to object
//returns a list of objects
function setStreet(arrIni) {
  arrIni.map((person) => {
    streetAssign.map((i) => {
      person.address.street = streetAssign[Math.round(Math.random()*streetAssign.length)];
    });
    //console.log(person.address.street);
    assignedList.push(Object.assign({}, person));
  });
  return assignedList;
}

//rename street if necessary
//returns a list of objects
function getStreet(arrObj){
  arrObj.map((person) => {
    streetOld.map((i) => {
      if (person.address.street == i){
        person.address.street = streetNew[streetOld.indexOf(i)];
      }});
      //console.log(person.address.street);
      newList.push(Object.assign({}, person));
    });
    return newList;
  }
  return getStreet(setStreet(arr));
}

console.log(renameStreet(students));
